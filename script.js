// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.

// При виклику функція повинна запитати ім'я та прізвище.

// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.

// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера,
// з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).

// Створити юзера за допомогою функції createNewUser().
// Викликати у цього юзера функцію getLogin().Вивести у консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму.
// Створити функції - сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

"use strict";

function createNewUser() {
  let newUser = {
    _firstName: prompt(`Input your first name:`),
    _lastName: prompt(`Input your last name:`),

    get firstName() {
      return this._firstName;
    },

    set firstName(newFirstName) {
      this._firstName = newFirstName;
    },

    get lastName() {
      return this._lastName;
    },

    set lastName(newLastName) {
      this._lastName = newLastName;
    },

    getLogin() {
      return (this._firstName.charAt(0) + this._lastName).toLowerCase();
    },
  };
  return newUser;
}

let user1 = createNewUser();

user1.getLogin();
